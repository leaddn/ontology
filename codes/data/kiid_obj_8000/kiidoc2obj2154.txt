Objectives and Investment Policy- The objective of the Fund is to maximise total return over a market cycle.- The Fund employs a multi-sector approach with a flexible allocation to credit rates and currencies.- The Fund invests primarily in debt instruments denominated in any currency (including but not limited to debt issued by companies, governments quasi- sovereigns and securitized debt).- The Fund intends to gain exposure to investment grade (higher quality) and non-investment grade (lower quality) debt instruments.- The Fund may invest in debt instruments which are in financial distress (distressed securities).- The Fund may also gain exposure to the loans market through derivatives and other eligible complex instruments.- The Fund may make significant use of derivatives (complex instruments) in order to (i) reduce the risk and/or generate additional capital or income and/or (ii) meet the Fund’s investment objectives by generating varying amounts of leverage (i.e. where the Fund gains market exposure in excess of the net asset value of the Fund).- The Fund is actively managed within its objectives and is not constrained by a benchmark.- You can buy and sell shares in the Fund on any Business Day in Luxembourg (as defined in the Prospectus).- Any income from your investment will be paid annually, out of gross income.