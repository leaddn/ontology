Objectives and Investment Policy- The objective of the Fund is to generate income and long-term capital appreciation.- The Fund will invest primarily in debt instruments (including contingent convertibles) issued and/or guaranteed by the Indian government or by Indian companies, and Indian money-market instruments.- The Fund is actively managed within its objectives and is not constrained by a benchmark.- You can buy and sell shares in the Fund on any Dealing Day (as defined in the Prospectus).- Any income from your investment will be paid monthly, out of gross income.