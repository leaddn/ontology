﻿java -cp termsuite-core-3.0.10.jar fr.univnantes.termsuite.tools.TerminologyExtractorCLI --from-text-corpus ./data/kiid_obj -t ./TreeTagger -l en --tsv ./session2/corpus_en/kiid_obj.tsv --tsv-properties "key,pattern,pilot,spec,freq,dFreq,ind"


java -cp termsuite-core-3.0.10.jar fr.univnantes.termsuite.tools.TerminologyExtractorCLI --from-text-corpus ./data/prosp_obj -t ./TreeTagger -l en --tsv ./session2/corpus_en/prosp_obj.tsv --tsv-properties "key,pattern,pilot,spec,freq,dFreq,ind"