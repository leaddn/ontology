#!/usr/bin/env python
# coding: utf-8

# # PDF Extraction

# ## Libraries

# In[2]:


get_ipython().system(' pip3 install pdfminer.six')


# In[1]:


import re
import sys
import os
import csv
import pandas as pd
import xlsxwriter
import pdftotext
from pdf_layout_scanner import layout_scanner
import time
import zipfile


# In[26]:


def get_list(user_input, path):
    
    if user_input == 'zip':
        with zipfile.ZipFile(path, 'r') as zipobj:
            names = [file for file in zipobj.namelist() if file.endswith("pdf")]
    else:
        names = [file for file in os.listdir(path) if file.endswith('pdf')]
    return names

def pdf_extraction(dir_out, file):
        '''Saves pdf in text file.'''
        
        
        content = ""
        suffix = ".csv"
        
        if os.path.isdir(path):
            with open(os.path.join(path, file), 'rb') as pdfFileObj:
                pdf = pdftotext.PDF(pdfFileObj)
                
                for page in pdf:
                    if "Schroder" in page:
                        pages=layout_scanner.get_pages(os.path.join(path, file))
                        for p in pages:
                            content += p
                            #content = re.sub(r"\n"," ",content)
                            #content = re.sub(r"Objectives and Investment Policy", "\n", content)
                    else:
                        content += page
        else:
            with zipfile.ZipFile(path, 'r') as zipobj:
                with zipobj.open(file) as pdfFileObj:
                    pdf = pdftotext.PDF(pdfFileObj)
                
                    for page in pdf:
                    #content += page

                        if "Schroder" in page:
                            pages=layout_scanner.get_pages(file)
                            for p in pages:
                                content += p
                                #content = re.sub(r"\n"," ",content)
                                #content = re.sub(r"Objectives and Investment Policy", "\n", content)
                        else:
                            content += page
            
            
        delete_list = ["[12]/2", "\s{3,}", "\n{2,}", "[a-z]\.\n+[a-z]"]
        for e in delete_list:
            content = re.sub(e, r"", content)
        content = re.sub(r"\n", " ", content)
        content = re.sub(r"([a-z])(\s{3,})([A-Z])", r"\1\r\n\3", content) #regex between 2 sentences to replace additional spaces by new line
        content = re.sub(r"(\.)(\s{3,})([A-Z])", r"\1\r\n\3", content) 

        list = ['Objectives and Investment Policy', 'Risk and Reward Profile', 'Charges'] #list of title's sections

        for e in list:
            content = re.sub(e, "\n"+e, content) #separate the sections with new line

        if "Schroder" in content:
            obj_list = ["Objectives", "Investment Policy", "Benchmark", "Dealing Frequency", "Distribution Policy"]
            for e in obj_list:
                content = re.sub(e, "\n"+e, content) #separate the sections with new line

                obj1 = re.findall(obj_list[0]+'.+', content) #regex to extract the whole objective section until the new section
                obj2 = re.findall(obj_list[1]+'.+', content)
                obj3 = re.findall(obj_list[2]+'.+', content)
                obj4 = re.findall(obj_list[3]+'.+', content)
                obj5 = re.findall(obj_list[4]+'.+', content)
        else:
            objectives = re.findall(list[0]+'.+', content) #regex to extract the whole objective section until the new section

        risks = re.findall(list[1]+'.+', content) #regex to extract the whole risk section

        subfund_text = re.findall("Key Investor Information"+'.+', content)

        content = re.sub(r"SICAV.", r"SICAV ", content)
        content = re.sub(r"\.", r".\n", content) # new line after each point



        # extract the subfund's names
        subfund = "(.+)(?: [aA] [sS]ubfund|[sS]ub-fund of )"
        umbrella = "(?: [aA] [sS]ubfund|[sS]ub-fund of )(.+)(?:[\,\(])"


        #for subfund in subfund_list:
        names = re.findall(subfund, content)
        umbrella_list = re.findall(umbrella, content)



        isin = 'LU\d+'

        isin_code = re.findall(isin, content) # extract ISIN code




        # for every kiidoc, build one dictionary with the following features:



        try:
            if "Schroder" in content:
                dictionary = dict(name_file=file, share_class=isin_code[0], subfund=names[0], umbrella_fund=umbrella_list[0], obj=obj1[0]+obj2[0]+obj3[0]+obj4[0]+obj5[0], risk=risks[0], subfund_desc=None)

            else:
                dictionary = dict(name_file=file, share_class=isin_code[0], subfund=names[0], umbrella_fund=umbrella_list[0], obj=objectives[0], risk=risks[0], subfund_desc=subfund_text[0])
            with open(outfile, 'w') as output:  
                w = csv.DictWriter(output, dictionary.keys())
                w.writeheader()
                w.writerow(dictionary)

        except:
            dictionary = dict(name_file=file, share_class=None, obj=None, risk=None, subfund_desc=None)
            #wrong_file.append(f)
            with open("wrong.txt", "w", encoding="utf-8") as wrong_f:
                wrong_f.write(str(file))

        print("\n"+"filename: "+file+"\n") 

        return dictionary

if __name__=="__main__":
    tic=time.perf_counter()
    user_input = input("Choose 'zip' or 'folder': ")
    path=input("path to zip folder: ")
    names=get_list(user_input, path)
    dir_out = input("path to output folder: ")
    i = 0
    chunk = 0
    while i < len(names):
        j = i + 100
        print("CHUNK : "+str(chunk))
        for file in names[i:j]:
            pdf_extraction(dir_out, file)
            print("file nb: "+str(names.index(file)))
        print("\n"+(str(len(names[i:j])))+" files extracted"+"\n")
        print("========================")
        
        chunk += 1
        i += 100
    toc = time.perf_counter()
    print(str(len(names))+f" extracted files in {toc - tic:0.4f} seconds")


# In[2]:


def dataframe(dir_dict):
    dic_list = [f for f in os.listdir(dir_dict) if f.endswith('.csv')]
    data_list = []
    for f in dic_list:
        df = pd.read_csv(os.path.join(dir_dict, f))
        data_list.append(df)
    df_list = pd.concat(data_list)
    #df = df_list
    return df_list

dir_dict = input("Path to csv files: ")
df = dataframe(dir_dict)
df.head()


# In[6]:


df.info()


# In[20]:


df['share_class'].describe()


# In[21]:


df['name_file'].describe()


df.shapedf.head(20)
# ## Data Cleaning

# In[3]:


import re
import nltk
from nltk import word_tokenize
#from nltk.corpus import stopwords
#from stop_words import get_stop_words

#stop_words = get_stop_words('en')
from nltk.stem import WordNetLemmatizer
lemmatizer = WordNetLemmatizer()

#SPACE_REPLACEMENT = re.compile('[/(){}\[\]\|@,;]')
SPACE_REPLACEMENT = re.compile('[/()]')
"""
SYMBOLS = re.compile('[^0-9a-z #+_]')
STOPWORDS = set(stopwords.words('english'))
pattern = r'''(?x)     # set flag to allow verbose regexps
     (?:[A-Z]\.)+       # abbreviations, e.g. U.S.A.
   | \w+(?:-\w+)*       # words with optional internal hyphens
   | \$?\d+(?:\.\d+)?%? # currency and percentages, e.g. $12.40, 82%
   | \.\.\.             # ellipsis
   | [][.,;"'?():-_`]   # these are separate tokens; includes ], [
 '''
to be deleted: 2/2,
"""


# In[4]:


def clean_text(text):
    """
        text: a string
        
        return: modified initial string
    """
    #text = text.lower() # lowercase text
    #text = ' '.join(nltk.regexp_tokenize(text, pattern))
    text = SPACE_REPLACEMENT.sub(' ', text) # replace SPACE_REPLACEMENT symbols by space in text. substitute the matched string in REPLACE_BY_SPACE_RE with space.
    #text = SYMBOLS.sub('', text) # remove symbols present in SYMBOLS from text. 
    #text = ' '.join(word for word in text.split() if word not in STOPWORDS) # remove stopwords from text
    #text = ' '.join(word for word in word_tokenize(text.lower()) if word not in STOPWORDS) # tokenize text
    #text = ' '.join(set([lemmatizer.lemmatize(word) for word in text]))
    text = re.sub(r"(\s{2,})|(\r)", " ", text)
    text = re.sub(r"(Objectives and Investment Policy)-?|(Risk and Reward Profile)", " ", text)

    text = re.sub(r".- ",". ", text)
    text = re.sub(r"and/or", "and or", text)
    return text


# In[5]:


def clean_name(text):
    text = re.sub(r",", "", text)
    text = re.sub(r"of the", "", text)
    text = re.sub(r" short named .+", "", text)
    text = re.sub(r'\(the .+', "", text)
    text = re.sub(r'SICAV', "", text)
    text = re.sub(r'[C ]lass.+', "", text)
    return text


# In[8]:


def drop(df):
    before = str(df.shape)
    print('shape of df before: ' + before)
    # delete blank rows and duplicated files
    df.columns = ["name_file","share_class", "subfund", "umbrella_fund", "objectives", "risks", "description"]

    df.dropna(inplace=True)
    df.drop_duplicates(subset="share_class", inplace=True)
    after = str(df.shape)
    print('shape of df after: ' + after)
    return df


# In[9]:


df = drop(df)

# delete laiding and trailing spaces
df['subfund'] = df['subfund'].str.strip()
df['umbrella_fund'] = df['umbrella_fund'].str.strip()
df['objectives'] = df['objectives'].str.strip()
df['risks'] = df['risks'].str.strip()
df['description'] = df['description'].str.strip()

# clean text
df['objectives'] = df['objectives'].apply(clean_text)
df['risks'] = df['risks'].apply(clean_text)
df['description'] = df['description'].apply(clean_text)

# specific cleaning
df['subfund'] = df['subfund'].apply(clean_name)
df['umbrella_fund'] = df['umbrella_fund'].apply(clean_name)

#df.head()


# In[22]:


def extract_company_group(df):
    company = [df.description.str.extract("(?:[mM]anaged by)(.+)(?:, part)")]
    company = company[0]
    group = [df.description.str.extract("(?:, part of the )(.+)")]
    group = group[0]
    company_group = pd.concat([company, group], axis=1)
    company_group.columns = ["company", "group"]
    df = pd.concat([df, company_group], axis=1)
    return df


# In[23]:


df = extract_company_group(df)
df.head()


# In[39]:


df.to_excel("./outputs/kiid8000/kiidoc2table.xlsx")


# def obj2text(df):
#     for i in range(0, len(df)):
# 
#         objective = df.iloc[i]['objectives']
#         with open(path+"kiidoc2obj"+str(i)+".txt", "w") as f:
#             f.write(objective)
#     return objective
#             
#             
# path = "./terminology/kiid_obj_8000/"
# objective = obj2text(path+file)

# ## Extract & Zip Objectives

# In[26]:


def extract_obj(df, path):

    #path = "./terminology/kiid_obj_8000/"

    for i in range(0, len(df)):

        objective = df.iloc[i]['objectives']
        with open(path+"kiidoc2obj"+str(i)+".txt", "w") as f:
            f.write(objective)
    return

zip_obj = extract_zip_obj(df, path="./out_test/para/")


# In[27]:



from zipfile import ZipFile


# In[28]:


def get_all_file_paths(directory): 
  
    # initializing empty file paths list 
    file_paths = [] 
  
    # crawling through directory and subdirectories 
    for root, directories, files in os.walk(directory): 
        for filename in files: 
            # join the two strings in order to form the full filepath. 
            filepath = os.path.join(root, filename) 
            file_paths.append(filepath) 
  
    # returning all file paths 
    return file_paths         
  
def main(directory): 
    # path to folder which needs to be zipped 
    #directory = directory
  
    # calling function to get all file paths in the directory 
    file_paths = get_all_file_paths(directory) 
  
    # printing the list of all files to be zipped 
    print('Following files will be zipped:') 
    for file_name in file_paths: 
        print(file_name) 
  
    # writing files to a zipfile 
    with ZipFile('kiid_obj.zip','w') as zip: 
        # writing each file one by one 
        for file in file_paths: 
            zip.write(file) 
  
    print('All files zipped successfully!')         
  
  
if __name__ == "__main__": 
    main(directory="./out_test/para/") 


# # Analysis :

# In[1]:


import pandas as pd
import numpy as np
from nltk.corpus import stopwords
from sklearn.metrics.pairwise import linear_kernel
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import LatentDirichletAllocation
import re
import random
import plotly.graph_objs as go
import chart_studio as py
import cufflinks
pd.options.display.max_columns = 30
from IPython.core.interactiveshell import InteractiveShell
import plotly.figure_factory as ff
InteractiveShell.ast_node_interactivity = 'all'
from plotly.offline import iplot
cufflinks.go_offline()
cufflinks.set_config_file(world_readable=True, theme='space')


# ## Load Data

# In[2]:


kiid2table = pd.read_excel("./outputs/kiid8000/kiidoc2table.xlsx")
kiid2table.head()


# In[14]:


umbrella = kiid2table.groupby("umbrella_fund")
umbrella.groups


# In[8]:


subfund = kiid2table.groupby("subfund")
subfund.groups


# In[15]:


for umb, group in umbrella:
    print(umb)
    print(group)


# In[4]:


def get_top_n_words(corpus, n=None):
    vec = CountVectorizer(stop_words='english').fit(corpus)
    bag_of_words = vec.transform(corpus)
    sum_words = bag_of_words.sum(axis=0) 
    words_freq = [(word, sum_words[0, idx]) for word, idx in vec.vocabulary_.items()]
    words_freq =sorted(words_freq, key = lambda x: x[1], reverse=True)
    return words_freq[:n]

common_words = get_top_n_words(kiid2table['objectives'], 20)
df2 = pd.DataFrame(common_words, columns = ['desc' , 'count'])
df2.groupby('desc').sum()['count'].sort_values().iplot(kind='barh', yTitle='Count', linecolor='black', title='Top 20 words in Investment objectives')


# In[5]:


common_words = get_top_n_words(kiid2table['risks'], 20)
df3 = pd.DataFrame(common_words, columns = ['desc' , 'count'])
df3.groupby('desc').sum()['count'].sort_values().iplot(kind='barh', yTitle='Count', linecolor='black', title='Top 20 words in Risks')


# In[6]:


def get_top_n_trigram(corpus, n=None):
    vec = CountVectorizer(ngram_range=(3, 3), stop_words='english').fit(corpus)
    bag_of_words = vec.transform(corpus)
    sum_words = bag_of_words.sum(axis=0) 
    words_freq = [(word, sum_words[0, idx]) for word, idx in vec.vocabulary_.items()]
    words_freq =sorted(words_freq, key = lambda x: x[1], reverse=True)
    return words_freq[:n]
common_words = get_top_n_trigram(kiid2table['objectives'], 20)
df4 = pd.DataFrame(common_words, columns = ['desc' , 'count'])
df4.groupby('desc').sum()['count'].sort_values(ascending=False).iplot(kind='bar', yTitle='Count', linecolor='black', title='Top 20 trigrams in Investment objectives')


# In[7]:


common_words = get_top_n_trigram(kiid2table['risks'], 20)
df5 = pd.DataFrame(common_words, columns = ['desc' , 'count'])
df5.groupby('desc').sum()['count'].sort_values(ascending=False).iplot(kind='bar', yTitle='Count', linecolor='black', title='Top 20 trigrams in Risks')


# In[ ]:




