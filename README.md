# Towards Ontology Learning from Investment Funds Data

By Léa Dieudonat, 03/2020 - 08/2020

This project takes place in the context of my M2 internship in Natural Language Processing.

The goal is to propose a semi-automatically learning approach to build an ontology from unstructured investment funds data.
This method aims to classify funds according to their investment objectives.

## Structure of the ReadMe:

- Abstract
- Dependencies
- Usage
- Repository
- Branches

## Abstract

Digitization in the finance industry has enabled technology to increase the production of data. The challenge with this transformation of financial institutions is for the companies to respond to this change and find a way to get data quality.
Because the data is unstructured or not captured within the firm, methods such as vocabularies, taxonomies, thesauri and topic maps and logical models are not sufficient.
Ontology, as a new popular method in the data transformation, expresses relationships and allow users to link concepts with other concepts in multiple ways.
As one of the building blocks of semantic technology, ontologies are part of the W3C standards stack for the semantic web. They are a key concept for data management as they ensure a common understanding of information and make explicit domain assumptions. They also improve metadata and provenance, helping organizations to understand data better and enhance data quality.
The main objective of this thesis was to investigate how an ontology can be used to overcome the above challenges and how to implement a system that uses ontology to assist auditors when making a decision.
This paper describes the process of ontology learning and its application in the financial field. It also explores the actual state of the art of ontology learning to propose a first proof of concept of semi-automatic ontology based on investment funds.

## Dependencies

This project was built with Python 3.7 and the following external packages (you can also use the requirements.txt file):
TODO : check version

- numpy version 1.17.3;
- pandas version 0.25.1;
- nltk version 3.4.5;
- spacy version 2.1.9, as well as its "en_core_web_sm" model, that you can get by running python -m spacy download en_core_web_sm;
- matplotlib version 3.1.1;
- sklearn version 0.21.3.

Also, the following native packages are used, so check that they are available:

- json;
- sys;
- re.

Other packages:
TODO: put bibliographic references

- termsuite
- OpenIE 5
- DBpedia Spotlight


## Usage
1 - objectives_extraction
    input: PDF files
    output: kiid2obj.txt files, kiid2table.xlsx (dataframe)
2 - Termsuite : terminology extraction
    input: kiid2obj.txt files
    output: kiid2terms.xlsx
3 - Mapping terms to Kiids
    input: kiid2terms.xlsx
    output: kiid2terms.xlsx
4 - LDA
    input: kiid2table.xlsx
    output: kiid2topic.xlsx, sent_topics.xlsx, wordclouds.png, word_counts.png, word_weights.png
5 - terms clustering
    input: kiid2topic.xlsx
    output: hierarchy_term.png, PCA_terms.png
6 - Relation extraction
    input: kiid2table.xlsx
    output: relations.xlsx
7 - Relation classification
    input: relations.xlsx
    output: relations_cleaned.xlsx
8 - Lipper
    input: lipper.xlsx, kiid2table.xlsx
    output: joined_lipper_lda.xlsx, asset_df_thres.xlsx (or other visualisations...)
9 - Document similarity
    input: kiid2table.xlsx, prosp2table.xlsx
    output: obj_perc.xlsx, merge_kiid_prosp.xlsx

## Repository

- ReadMe
- codes
-   terminology
-   data: directory for input data
-   outputs: directory for results and code outputs
- biblio: directory for the papers/article used in this project
- reports : directory for the report and slides
- weekly reports: directory for the weekly reports



